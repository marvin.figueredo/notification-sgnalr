﻿using Microsoft.AspNet.SignalR;
using NotificationSignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationSignalR.Helpers
{
    public static class Functions
    {
        public static void Enviarnotificacion(string message, string user_name)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificacionesHub>();

            if (string.IsNullOrWhiteSpace(user_name))
            {
                hubContext.Clients.All.showClientNotification(message);
            }
            else
            {
                hubContext.Clients.User(user_name).showClientNotification(message);
            }
        }
    }
}