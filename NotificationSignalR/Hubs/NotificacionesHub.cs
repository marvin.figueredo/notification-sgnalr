﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationSignalR.Hubs
{
    public class NotificacionesHub: Hub
    {
        public void Enviarnotificacion(string message, string user_name)
        {
            if (string.IsNullOrWhiteSpace(user_name))
            {
                Clients.All.showClientNotification(message);
            }
            else
            {
                Clients.User(user_name).showClientNotification(message);
            }
        }
    }
}