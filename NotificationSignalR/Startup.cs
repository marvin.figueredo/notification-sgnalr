﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NotificationSignalR.Startup))]
namespace NotificationSignalR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
