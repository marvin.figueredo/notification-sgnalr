﻿using NotificationSignalR.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotificationSignalR.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult Notificar(string mensaje, string user_name)
        {
            Functions.Enviarnotificacion($"Mensaje de {User.Identity.Name}: - {mensaje} ", user_name);

            return Json(new { Success = true });
        }
    }
}