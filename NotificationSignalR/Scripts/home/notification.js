﻿$(function () {
    var my_hub = $.connection.notificacionesHub;

    $.connection.hub.start().done(function () {
        var connectionId = $.connection.hub.id;
        console.log(connectionId);
    });

    $("#btnNotificar").click(function () {
        EnviarNotificacion(my_hub);
    });

    my_hub.client.showClientNotification = function (message) {
        $("#alert-title").text('Notificación! ');
        $("#alert-msg").text(message);
        $("#Alert").show();
    }

    $("#btnNotificarDesdeServidor").click(function () {
        EnviarNotificacionDesdeServidor();
    });
});

function EnviarNotificacion(my_hub) {
    var msg = $("#txtMensaje").val();
    var user_name = $("#txtDestinatario").val();
    my_hub.server.enviarnotificacion(msg, user_name);
}

function EnviarNotificacionDesdeServidor() {
    var msg = $("#txtMensaje").val();
    var user_name = $("#txtDestinatario").val();

    $.ajax({
        method: 'POST',
        url: '/home/Notificar',
        data: { mensaje: msg, user_name: user_name }
    })
        .done(function (data) {
            if (!data.Success) {
                alert('Ocurrió un error...');
            }
        });
}